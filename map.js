// add an OpenStreetMap tile layer
var osmLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
  minZoom: 11,
  maxZoom: 16
});

var current_wards = L.tileLayer('current_wards/{z}/{x}/{y}.png', {
  attribution: 'City of Ann Arbor',
  minZoom: 11,
  maxZoom: 16
});

var prop_wards = L.tileLayer('prop_wards/{z}/{x}/{y}.png', {
  attribution: 'The Michigan Daily',
  minZoom: 11,
  maxZoom: 16
});

var student_voters = L.tileLayer('aa_stu_voters/{z}/{x}/{y}.png', {
  attribution: 'Washtenaw County Clerk',
  minZoom: 11,
  maxZoom: 16
});

var map = L.map('map', {
  center: [42.2755, -83.7405],
  zoom: 11,
  layers: [osmLayer, current_wards, student_voters]
});

var southWest = L.latLng(42.2173, -83.8229),
    northEast = L.latLng(42.3306, -83.6506),
    geo_bounds = L.latLngBounds(southWest, northEast);

var options = {
  collapsed: false,
  bounds: geo_bounds
}

map.setMaxBounds(geo_bounds);

var wardMaps = {
  "Current Wards": current_wards,
  "Proposed Wards": prop_wards
};

var studentMap = {
  "Student Voters": student_voters
};

L.control.layers(wardMaps, studentMap).addTo(map);
